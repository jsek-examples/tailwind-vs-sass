# Comparison of Tailwind CSS v3 and pure Sass

Inspired by short video about [breakpoints in Tailwind v3](https://youtu.be/uPcaxF6ktZY)

## Run on localhost

```sh
> npm i
> npm run dev
```